'use strict';
exports.__esModule = true;
var header_types_1 = require('./header-types');
function createInteger(numbers) {
  return numbers.reduce(function(final, n) {
    return (final << 1) | n;
  }, 0);
}
function createColor(color) {
  return (
    (color.quad << 24) | (color.red << 16) | (color.green << 8) | color.blue
  );
}
var BmpEncoder = /** @class */ (function() {
  function BmpEncoder(imgData) {
    this.buffer = imgData.data;
    this.width = imgData.width;
    this.height = imgData.height;
    this.headerSize = header_types_1['default'].BITMAP_INFO_HEADER;
    // Header
    this.flag = 'BM';
    this.bitPP = imgData.bitPP;
    this.offset = 54;
    this.reserved1 = imgData.reserved1 || 0;
    this.reserved2 = imgData.reserved2 || 0;
    this.planes = 1;
    this.compress = 0;
    this.hr = imgData.hr || 0;
    this.vr = imgData.vr || 0;
    this.importantColors = imgData.importantColors || 0;
    this.colors = Math.min(
      Math.pow(2, this.bitPP - 1 || 1),
      imgData.colors || Infinity
    );
    this.palette = imgData.palette || [];
    if (this.colors && this.bitPP < 16) {
      this.offset += this.colors * 4;
    } else {
      this.colors = 0;
    }
    switch (this.bitPP) {
      case 32:
        this.bytesInColor = 4;
        break;
      case 16:
        this.bytesInColor = 2;
        break;
      case 8:
        this.bytesInColor = 1;
        break;
      case 4:
        this.bytesInColor = 1 / 2;
        break;
      case 1:
        this.bytesInColor = 1 / 8;
        break;
      default:
        this.bytesInColor = 3;
        this.bitPP = 24;
    }
    var rowWidth = (this.width * this.bitPP) / 32;
    var rowBytes = Math.ceil(rowWidth);
    this.extraBytes = (rowBytes - rowWidth) * 4;
    // Why 2?
    this.rawSize = this.height * rowBytes * 4 + 2;
    this.fileSize = this.rawSize + this.offset;
    this.data = Buffer.alloc(this.fileSize, 0x1);
    this.pos = 0;
    this.encode();
  }
  BmpEncoder.prototype.encode = function() {
    this.pos = 0;
    this.writeHeader();
    switch (this.bitPP) {
      case 32:
        this.bit32();
        break;
      case 16:
        this.bit16();
        break;
      case 8:
        this.bit8();
        break;
      case 4:
        this.bit4();
        break;
      case 1:
        this.bit1();
        break;
      default:
        this.bit24();
    }
  };
  BmpEncoder.prototype.writeHeader = function() {
    this.data.write(this.flag, this.pos, 2);
    this.pos += 2;
    this.writeUInt32LE(this.fileSize);
    // Writing 2 UInt16LE resulted in a weird bug
    this.writeUInt32LE((this.reserved1 << 16) | this.reserved2);
    this.writeUInt32LE(this.offset);
    this.writeUInt32LE(this.headerSize);
    this.writeUInt32LE(this.width);
    this.writeUInt32LE(this.height);
    this.data.writeUInt16LE(this.planes, this.pos);
    this.pos += 2;
    this.data.writeUInt16LE(this.bitPP, this.pos);
    this.pos += 2;
    this.writeUInt32LE(this.compress);
    this.writeUInt32LE(this.rawSize);
    this.writeUInt32LE(this.hr);
    this.writeUInt32LE(this.vr);
    this.writeUInt32LE(this.colors);
    this.writeUInt32LE(this.importantColors);
  };
  BmpEncoder.prototype.bit1 = function() {
    var _this = this;
    if (this.palette.length && this.colors === 2) {
      this.initColors(1);
    } else {
      this.writeUInt32LE(0x00ffffff); // Black
      this.writeUInt32LE(0x00000000); // White
    }
    this.pos += 1; // ?
    var lineArr = [];
    this.writeImage(function(p, index, x) {
      var i = index;
      i++;
      var b = _this.buffer[i++];
      var g = _this.buffer[i++];
      var r = _this.buffer[i++];
      var brightness = r * 0.2126 + g * 0.7152 + b * 0.0722;
      lineArr.push(brightness > 127 ? 0 : 1);
      if ((x + 1) % 8 === 0) {
        _this.data[p - 1] = createInteger(lineArr);
        lineArr = [];
      } else if (x === _this.width - 1 && lineArr.length > 0) {
        _this.data[p - 1] = createInteger(lineArr) << 4;
        lineArr = [];
      }
      return i;
    });
  };
  BmpEncoder.prototype.bit4 = function() {
    var _this = this;
    var colors = this.initColors(4);
    var integerPair = [];
    this.writeImage(function(p, index, x) {
      var i = index;
      var colorInt = createColor({
        quad: _this.buffer[i++],
        blue: _this.buffer[i++],
        green: _this.buffer[i++],
        red: _this.buffer[i++]
      });
      var colorExists = colors.findIndex(function(c) {
        return c === colorInt;
      });
      if (colorExists !== -1) {
        integerPair.push(colorExists);
      } else {
        integerPair.push(0);
      }
      if ((x + 1) % 2 === 0) {
        _this.data[p] = (integerPair[0] << 4) | integerPair[1];
        integerPair = [];
      }
      return i;
    });
  };
  BmpEncoder.prototype.bit8 = function() {
    var _this = this;
    var colors = this.initColors(8);
    this.writeImage(function(p, index) {
      var i = index;
      var colorInt = createColor({
        quad: _this.buffer[i++],
        blue: _this.buffer[i++],
        green: _this.buffer[i++],
        red: _this.buffer[i++]
      });
      var colorExists = colors.findIndex(function(c) {
        return c === colorInt;
      });
      if (colorExists !== -1) {
        _this.data[p] = colorExists;
      } else {
        _this.data[p] = 0;
      }
      return i;
    });
  };
  BmpEncoder.prototype.bit16 = function() {
    var _this = this;
    this.writeImage(function(p, index) {
      var i = index + 1;
      var b = _this.buffer[i++] / 8; // b
      var g = _this.buffer[i++] / 8; // g
      var r = _this.buffer[i++] / 8; // r
      var color = (r << 10) | (g << 5) | b;
      _this.data[p] = color & 0x00ff;
      _this.data[p + 1] = (color & 0xff00) >> 8;
      return i;
    });
  };
  BmpEncoder.prototype.bit24 = function() {
    var _this = this;
    this.writeImage(function(p, index) {
      var i = index + 1;
      _this.data[p] = _this.buffer[i++]; //b
      _this.data[p + 1] = _this.buffer[i++]; //g
      _this.data[p + 2] = _this.buffer[i++]; //r
      return i;
    });
  };
  BmpEncoder.prototype.bit32 = function() {
    var _this = this;
    this.writeImage(function(p, index) {
      var i = index;
      _this.data[p + 3] = _this.buffer[i++]; // a
      _this.data[p] = _this.buffer[i++]; // b
      _this.data[p + 1] = _this.buffer[i++]; // g
      _this.data[p + 2] = _this.buffer[i++]; // r
      return i;
    });
  };
  BmpEncoder.prototype.writeImage = function(writePixel) {
    var rowBytes = this.extraBytes + this.width * this.bytesInColor;
    var i = 0;
    for (var y = 0; y < this.height; y++) {
      for (var x = 0; x < this.width; x++) {
        var p = Math.floor(
          this.pos + (this.height - 1 - y) * rowBytes + x * this.bytesInColor
        );
        i = writePixel.call(this, p, i, x, y);
      }
    }
  };
  BmpEncoder.prototype.initColors = function(bit) {
    var colors = [];
    if (this.palette.length) {
      for (var i = 0; i < this.colors; i++) {
        var rootColor = createColor(this.palette[i]);
        this.writeUInt32LE(rootColor);
        colors.push(rootColor);
      }
    } else {
      throw new Error(
        'To encode ' +
          bit +
          '-bit BMPs a pallette is needed. Please choose up to ' +
          this.colors +
          ' colors. Colors must be 32-bit integers.'
      );
    }
    return colors;
  };
  BmpEncoder.prototype.writeUInt32LE = function(value) {
    this.data.writeUInt32LE(value, this.pos);
    this.pos += 4;
  };
  return BmpEncoder;
})();
exports['default'] = BmpEncoder;
//# sourceMappingURL=encoder.js.map
