/// <reference types="node" />
import BmpDecoder from './decoder';
import BmpEncoder from './encoder';
declare const _default: {
    decode: (bmpData: Buffer, options?: IDecoderOptions | undefined) => BmpDecoder;
    encode: (imgData: IImage) => BmpEncoder;
};
export default _default;
