'use strict';
exports.__esModule = true;
var HeaderTypes;
(function(HeaderTypes) {
  HeaderTypes[(HeaderTypes['BITMAP_INFO_HEADER'] = 40)] = 'BITMAP_INFO_HEADER';
  HeaderTypes[(HeaderTypes['BITMAP_V2_INFO_HEADER'] = 52)] =
    'BITMAP_V2_INFO_HEADER';
  HeaderTypes[(HeaderTypes['BITMAP_V3_INFO_HEADER'] = 56)] =
    'BITMAP_V3_INFO_HEADER';
  HeaderTypes[(HeaderTypes['BITMAP_V4_HEADER'] = 108)] = 'BITMAP_V4_HEADER';
  HeaderTypes[(HeaderTypes['BITMAP_V5_HEADER'] = 124)] = 'BITMAP_V5_HEADER';
})(HeaderTypes || (HeaderTypes = {}));
exports['default'] = HeaderTypes;
//# sourceMappingURL=header-types.js.map
