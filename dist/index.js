'use strict';
exports.__esModule = true;
var decoder_1 = require('./decoder');
var encoder_1 = require('./encoder');
exports['default'] = {
  decode: function(bmpData, options) {
    return new decoder_1['default'](bmpData, options);
  },
  encode: function(imgData) {
    return new encoder_1['default'](imgData);
  }
};
//# sourceMappingURL=index.js.map
